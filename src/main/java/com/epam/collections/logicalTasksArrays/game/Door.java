package com.epam.collections.logicalTasksArrays.game;

class Door {
    int power;

    int getPower() {
        return power;
    }

    void givePower(Hero hero) {
        hero.setHeroPower(power + hero.getHeroPower());
    }

    void reducePower() {
        power = 0;

    }
}
