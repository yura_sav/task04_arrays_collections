package com.epam.collections.logicalTasksArrays.game;


import static com.epam.collections.logicalTasksArrays.game.Const.HERO_POWER;

class Hero {
    private int heroPower;

    Hero() {
        heroPower = HERO_POWER;
    }

    int getHeroPower() {
        return heroPower;
    }

    void setHeroPower(int heroPower) {
        this.heroPower = heroPower;
    }
}
