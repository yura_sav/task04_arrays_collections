package com.epam.collections.logicalTasksArrays.game;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.collections.logicalTasksArrays.game.Const.NUMBER_OF_DOORS;
import static com.epam.collections.logicalTasksArrays.game.Const.RANDOM;

public class Arena {
    private static Hero hero = new Hero();
    private static Logger logger = LogManager.getLogger(Arena.class);

    public static void main(String[] args) {
        Door[] doors = new Door[NUMBER_OF_DOORS];
        fillUpDoor(doors);
        printDoors(doors);
        logger.info(countHowManyDoorsCanKill(doors) + " doors can kill you");
        System.out.println("------------------------------\n");
        findWay(doors);
    }

    private static void fillUpDoor(Door[] doors) {
        for (int i = 0; i < doors.length; i++) {
            if (RANDOM.nextDouble() < 0.5) {
                doors[i] = generateArtifact();
            } else {
                doors[i] = generateMonster();
            }
        }
    }

    private static void printDoors(Door[] doors) {
        for (Door door : doors) {
            System.out.println(door);
        }
    }


    private static Monster generateMonster() {
        return new Monster();
    }

    private static Artifact generateArtifact() {
        return new Artifact();
    }

    private static int countHowManyDoorsCanKill(Door[] doors) {
        int count = 0;
        for (Door door : doors) {
            if (door instanceof Monster && door.getPower() > hero.getHeroPower()) {
                count++;
            }
        }
        return count;
    }

    private static void findWay(Door[] doors) {
        int i = 0;
        int count = 0;
        while (true) {
            for (Door door : doors) {
                i++;
                if (door instanceof Artifact && door.getPower() != 0) {
                    logger.info("door N " + i + " you will get artifact with power: " + door.getPower());
                    door.givePower(hero);
                    door.reducePower();
                } else if (door instanceof Monster && door.getPower() != 0) {
                    if (hero.getHeroPower() < door.getPower()) {
                        logger.info("you haven't enough power: " + hero.getHeroPower());

                    } else {
                        logger.info("door N " + i + " you killed monster, your power: " + hero.getHeroPower());
                        door.reducePower();
                    }
                }
            }
            if (checkAllDoors(doors)) {
                i = 0;
                ++count;
                if (count >= 2) {
                    logger.info("you can't win");
                    break;
                }
            } else {
                logger.info("end of the game you win");
                break;
            }
        }
    }

    private static boolean checkAllDoors(Door[] doors) {
        for (Door door : doors) {
            if (door.getPower() != 0) {
                return true;
            }
        }
        return false;
    }
}
