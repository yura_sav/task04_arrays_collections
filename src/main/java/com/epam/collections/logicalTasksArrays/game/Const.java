package com.epam.collections.logicalTasksArrays.game;

import java.util.Random;

final class Const {
    static final int HERO_POWER = 25;
    static final int NUMBER_OF_DOORS = 10;
    static final int MIN_MONSTER_POWER = 5;
    static final int MAX_MONSTER_POWER = 100;
    static final int MIN_ARTIFACT_POWER = 5;
    static final int MAX_ARTIFACT_POWER = 100;
    static final Random RANDOM = new Random();

    private Const() {

    }
}
