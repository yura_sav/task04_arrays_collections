package com.epam.collections.myDeque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DequeExample {
    private static Logger logger = LogManager.getLogger(DequeExample.class);

    public static void main(String[] args) {
        MyDeque<Integer> myDeque = new MyDeque<>(2);
        myDeque.addFirst(1);
        myDeque.addLast(3);
        myDeque.addFirst(2);
        myDeque.addFirst(116);
        myDeque.addFirst(1);
        logger.info(myDeque.remove(116));
        logger.info(myDeque.contains(7));
        myDeque.print();
        System.out.println("-------------------------");
        myDeque.clear();
        //System.out.println(myDeque.isEmpty());
    }
}
