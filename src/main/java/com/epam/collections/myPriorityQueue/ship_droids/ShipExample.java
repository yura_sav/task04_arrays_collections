package com.epam.collections.myPriorityQueue.ship_droids;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class ShipExample {
    private static Logger logger = LogManager.getLogger(ShipExample.class);

    public static void main(String[] args) {
        List<Droid> droids = Arrays.asList(new MilitaryDroid("M23", 100, 20), new HelperDroid("WASH", 20, true));
        Ship<Droid> ship = new Ship<>();
        List<? super Droid> list = ship.getMilitaryDroids(droids);
        logger.info(list);
        logger.info(ship.findMilitaryDroid(droids, new MiniMilitaryDroid("M23", 10, 20)));
    }
}
