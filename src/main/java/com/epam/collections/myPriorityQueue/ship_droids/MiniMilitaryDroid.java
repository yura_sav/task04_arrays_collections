package com.epam.collections.myPriorityQueue.ship_droids;

public class MiniMilitaryDroid extends MilitaryDroid {

    MiniMilitaryDroid(String name, int speed, int damage) {
        super(name, speed, damage);
    }

    @Override
    public String toString() {
        return "MiniMilitaryDroid";
    }
}
