package com.epam.collections.stringContainer.country;

public class Country {
    private String country;
    private String capital;

    public Country(String country, String capital) {
        if (country.equals("") ||
                capital.equals("")) {
            throw new NullPointerException();
        }
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public String toString() {
        return "Country{" +
                "country='" + country + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }

}

